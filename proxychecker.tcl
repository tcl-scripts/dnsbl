# Small vpn/proxy checker
#
# Think to fill your keys
# and choose your API
# Providers:
# proxycheck.io, whatismyip.com, ip-api.com, abuseipdb
 
namespace eval pchecker {
   
   # api keys
   variable keys {
      "proxycheck" "xxxxxx-xxxxxx-xxxxxx-xxxxxxxx" \
      "whatismyip" "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" \
      "abuseipdb" "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" \
      "ip-api" "" # No need of key
   }

   variable provider "ip-api"
   
   # min score to ban - Only used with proxycheck
   variable score 10

   # gzline message
   variable gmsg "Sorry, VPN are not allowed"

   # List of IP not checked
   # they are regexp style
   variable whitelist {"192\.168\.0\.1" "10\.0\.0\.*"}

   # List of blacklisted IP
   # regexp too :)
   variable blacklist {}
   
   # Connection string as seen by eggdrop
   variable connreg {Client connecting:\s([^\s]+)\s\(([^\)]+)\)\s\[([^\]]+)\]}

   # WARNING: Do not edit below

   package require http
   package require tls
   package require json
   
   variable akeys
   variable author "CrazyCat <https://www.eggdrop.fr>"
   variable version 1.2

   proc isvpn {frm key text} {
      if {[string match *!*@* $frm] || ![string match -nocase "*client connecting*" $text]} {
         return
      }
      if {![regexp $::pchecker::connreg $text - nick uhost ip]} {
         return
      }
      if {[lsearch -regexp $::pchecker::whitelist $ip] ne -1} { return }
      if {[lsearch -regexp $::pchecker::blacklist $ip] ne -1} {
         putquick "GLINE *@$ip +7d :$::pchecker::gmsg"
         return
      }
      putlog "Testing $ip ($nick - $uhost)"
      ::http::config -useragent "lynx"
      switch $::pchecker::provider {
         "proxycheck" { set danger [::pchecker::proxycheck $ip] }
         "ip-api" { set danger [::pchecker::ipapi $ip] }
         "whatismyip" { set danger [::pchecker::whatismyip $ip] }
         "abuseipdb" { set danger [::pchecker::abuseipdb $ip] }
         default { return }
      }
      if {$danger ne 0} {
         lappend $::pchecker::blacklist [string map {\. \\\.} $danger]
         putquick "GLINE *@$danger +7d :$::pchecker::gmsg"
      }
   }
   
   proc ipapi {ip} {
      set pcheck [::http::geturl http://ip-api.com/json/${ip}?fields=status,message,countryCode,proxy,query]
      set data [::json::json2dict [::http::data $pcheck]]
      ::http::cleanup $pcheck
      if {[dict get $data status] eq "success"} {
         set proxy [dict get $data proxy]
         set country [dict get $data countryCode]
         if {$proxy eq "true"} {
            return $ip
         }
      }
      return 0
   }
   
   proc proxycheck {ip} {
      set pcheck [::http::geturl http://proxycheck.io/v2/${ip}?key=$::pchecker::akeys(proxycheck)&vpn=3&risk=1]
      set data [::json::json2dict [::http::data $pcheck]]
      ::http::cleanup $pcheck
      if {[dict get $data status] eq "ok"} {
         set proxy [dict get [dict get $data $ip] proxy]
         set vpn [dict get [dict get $data $ip] vpn]
         set risk [dict get [dict get $data $ip] risk]
         if {($vpn eq "yes" || $proxy eq "yes") && [expr $risk - $::pchecker::score] >= 0 } {
            return $ip
         }
      }
      return 0
   }
   
   proc whatismyip {ip} {
      ::http::register https 443 [list ::tls::socket -autoservername true]
      set pcheck [::http::geturl https://api.whatismyip.com/proxy.php?key=$::pchecker::akeys(whatismyip)&input=$ip&output=json]
      set data [::json::json2dict [::http::data $pcheck]]
      set data2 [lindex [dict get $data "proxy-check"] 1]
      ::http::cleanup $pcheck
      if {[dict get [lindex [dict get $data "proxy-check"] 0] status] eq "ok"} {
         set proxy [dict get $data2 is_proxy]
         set ptype [dict get $data2 proxy_type]
         if {$proxy ne "no"} {
            return $ip
         }
      }
      return 0
   }
   
   proc abuseipdb {ip} {
      ::http::register https 443 [list ::tls::socket -autoservername true]
      set pcheck [::http::geturl https://api.abuseipdb.com/api/v2/check?ipAddress=$ip -headers [list "Accept" "application/json" "Key" $::pchecker::akeys(abuseipdb)]]
      set data [::json::json2dict [::http::data $pcheck]]
      ::http::cleanup $pcheck
      if {[dict exists $data data]} {
         set data [dict get $data data]
         if {[dict get $data abuseConfidenceScore] > 25 || [dict get $data isTor] eq "true"} {
            return $ip
         }
      }
      return 0
   }
   
   proc init {} {
      foreach {p k} $::pchecker::keys {
         set ::pchecker::akeys($p) $k
      }
      bind raw - NOTICE ::pchecker::isvpn
      putlog "Proxychecker v${::pchecker::version} by ${::pchecker::author} loaded"
   }
   
   ::pchecker::init
   
}
