namespace eval dnsbl {

	variable dnsfile "scripts/dnsbl.json"
	variable checkers
	
	if {![file exists $::dnsbl::dnsfile]} { putloglev o * "DNSBL Error : dnsbl file not found, aborting"; return }
	package require ip
	if {[catch {package require json}]} {
		putloglev o * "DNSBL Warning : json package not found, using hack..."
		namespace eval json {
			proc json2dict {JSONtext} {
				string range [string trim [string trimleft [string map {\t {} \n {} \r {} , { } : { } \[ \{ \] \}} $JSONtext] {\uFEFF}]] 1 end-1
			}
		}
	}
	if {[catch {set fi [open $::dnsbl::dnsfile r]}]} {
		putloglev o * "DNSBL Error : $::dnsbl::dnsfile doesn't exists or is not readable."
		return
	}
	if {[catch {set ::dnsbl::checkers [::json::json2dict [read -nonewline $fi]]}]} {
		putloglev o * "DNSBL Error : Bad format for $::dnsbl::dnsfile"
		return
	}

	bind pub - !check ::dnsbl::pubcheck
	proc pubcheck { nick uhost handle chan text } {
		regexp -all {(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])} $text m a b c
		if {![info exists m] || ($m ne $text)} {
			if {[string first : $text]>-1} {
				# ipv6
				::dnsbl::docheck6 $text $chan
			} else {
				dnslookup $text ::dnsbl::h2ip $text $chan
			}
		} else {
			::dnsbl::docheck $text $chan
		}
	}

	proc h2ip {ip host status ohost chan} {
		putserv "PRIVMSG $chan :$ip $host gives $status ($ohost)"
		if {$status != 0} {
			::dnsbl::docheck $ip $chan
		} else {
			putserv "PRIVMSG $chan :$host is not an host"
		}
	}
	
	proc docheck {ip chan} {
		regsub {(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})} $ip {\4.\3.\2.\1} ip2
		foreach dns [dict keys $::dnsbl::checkers] {
			set server [dict get $::dnsbl::checkers $dns dns]
			putlog "Running lookup on $ip2.$server ($dns)"
			dnslookup "$ip2.$server" ::dnsbl::getcheck $dns $chan
		}
	}
	
	proc docheck6 {ip chan} {
		set ip [::ip::normalize $ip]
		putlog "Normalized : $ip"
		regsub -all -- {:} $ip {} ip
		putlog "no : $ip"
		set iip {}
		for {set i 0} {$i<=[string length $ip]} {incr i} {
			lappend iip [string index $ip end-$i]
		}
		set ip2 [join $iip .]
		putlog "Inverted : $ip2"
		foreach dns [dict keys $::dnsbl::checkers] {
			if {![dict exists $::dnsbl::checkers $dns ipv6] || [dict get $::dnsbl::checkers $dns ipv6] ne true} { continue }
			set server [dict get $::dnsbl::checkers $dns dns]
			putlog "Running lookup on $ip2$server ($dns)"
			dnslookup "$ip2$server" ::dnsbl::getcheck $dns $chan
		}
	}

	proc getcheck {ip host status dns chan} {
		if {!$status || $status==0} { return }
		set from [split [dict get $::dnsbl::checkers $dns reply] ;]
		if {[lsearch $status $from]!=-1} {
			putserv "PRIVMSG $chan :$dns gives status $status for $ip"
		} else {
			putserv "PRIVMSG $chan :$dns replies $status for $ip, but allowed"
		}
	}
}